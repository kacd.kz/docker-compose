Для начала создаём 2 сервиса app и db.  
Создаём отдельные директории для них и создаём для них свой Dockerfile.  

Для Докер сервиса с базой данных Postgres создаём докерфайл со следующими переменными  
которые будут использоваться при подключений сервисом app.   
FROM postgres:latest  
ENV POSTGRES_PASSWORD=secret  
ENV POSTGRES_USER=username  
ENV POSTGRES_DB=database  

Для Докер сервиса с приложением создаём докерфайл со следующими значениями  
    --образ python 3.8  
    --определяем директорию для запуска   
    --копируем всё из директорий app в контейнер    
    --устанавливаем зависимости   
    --задаём переменную необходимую для запуска приложения  
    --bash скрип запуска приложения делаем исполняемым  
    --задаём запускаемый скрипт после запуска контейнера  
  

FROM python:3.8-slim    
WORKDIR /app         
ENV FLASK_APP=src/app.py   
COPY . .     
RUN pip install -r requirements.txt   
COPY ./docker-entrypoint.sh /    
RUN chmod +x /docker-entrypoint.sh  

ENTRYPOINT ["/docker-entrypoint.sh"]  


Теперь создаём Docker-compose.yml файл для запуска обоих сервисов. При обращений к друг другу сервисы использует заданные имена сервисов.  

services:  
  app:  
    build: ./app  
    restart: always  -- добавил ключ после того, как увидел что базе данных требуется время для запуска.  
    environment:     -- задаём переменные необходимые для подключения  
      DB_NAME: database  
      DB_USER: username  
      DB_PASSWORD: secret  
      DB_HOST: db  
      FLASK_ENV: development  
    ports:   
     - '8000:8000'     -- порт маппинг для тестов снаружи  
  db:  
    build: ./db/  
 

 По условиям необходимо чтобы приложение обращалось к volumes с хоста. Это можно сделать добавив ключи volumes.
 
 Пока что работаю по уменьшению размера образов до 150мб. _____________

 Если использовать образ с докерхаба 3.8 slim, образ выйдет размером в 162 мб.  

Попробовал использовать лёгковесный alpine, но в нём не запускалось установка модуля psycopg2 из requirements.  
Не хватало библиотек от postgres для запуска.  Нашёл статью с решением проблемы, установкой библиотек при сборке образа.  
https://stackoverflow.com/questions/46711990/error-pg-config-executable-not-found-when-installing-psycopg2-on-alpine-in-dock

Собственно удалось добиться размера образа в 120МБ для сервиса app. 

Докерфайл для приложения. Устанавливаются дополнительные библиотеки и удаляются из кеша purge-м после установки requirements.

FROM python:3.8-alpine  
WORKDIR /app  
ENV FLASK_APP=src/app.py  
COPY . .  
RUN \  
 apk add --no-cache python3 postgresql-libs && \  
 apk add --no-cache --virtual .build-deps gcc python3-dev musl-dev postgresql-dev && \  
 python3 -m pip install -r requirements.txt --no-cache-dir && \  
 apk --purge del .build-deps && \
 apk --no-cache add curl    
COPY ./docker-entrypoint.sh /  
RUN chmod +x /docker-entrypoint.sh  

ENTRYPOINT ["/docker-entrypoint.sh"]  






